import React, { Component } from 'react';
import './App.css';
import Home from './Home';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import CompaniesList from './CompaniesList';
import EmployeeList from './EmployeeList';
import CompanyEdit from './CompanyEdit';
import EmployeeEdit from './EmployeeEdit';

class App extends Component {
  render() {
    return (
      <Router>
        <Switch>
          <Route path='/' exact={true} component={Home}/>
          <Route path='/companies' exact={true} component={CompaniesList}/>
          <Route path='/employees' exact={true} component={EmployeeList} />
          <Route path='/companies/:id' component={CompanyEdit}/>
          <Route path='/employees/:id' component={EmployeeEdit}/>
        </Switch>
      </Router>
    )
  }
}

export default App;